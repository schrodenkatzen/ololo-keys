; Copyright (c) Paul R. Tagliamonte <tag@pault.ag>, 2013 under the terms of
; hy.

(import [flask [Flask render-template jsonify redirect]])
(import json random)
(import os binascii hashlib base58 cryptos [web3.auto [*]])

(setv app (Flask "__main__" :static_url_path "/static"))  ; long story, needed hack
(setv b (cryptos.Bitcoin)
      )
(setv BTC_PER_PAGE 128
      BTC_LAST_PAGE 904625697166532776746648320380374280100293470930272690489102837043110636675
      ETHERSCAN_API "HUQEM3CS4AZDGBH2MKMSKYS859CPAK7NS4")
(defn urandom-seed[n &optional [l 32][mode list]] 
  (setv arr [])
  (for [i (range 0 l)]
    (setv step (int (pow 255 (- (- l 1) i))))
    (setv times (int (/ n step)))
    (if (> times 0) (do (arr.append times) (setv n (- n (* times step))) )
      (arr.append 0))
    )
  (if (= mode str) (.join "" (list (map chr arr))) arr))
(defn big-chr[num] (setv arr [])
  (while num
    (setv r (% num 256))
    (arr.append r)
    (setv num (- num r))
    (setv num (/ num 256)))
  (print arr)
  (.join "" (list (map (fn[x] (-> x int chr)) arr))))
(defn bitcoin-address-from-public-key[pkey](setv cmd (-> "timeout 10s python3 bitcoin-in-tiny-pieces/bitcoin-address-from-public-key.py {}" (.format pkey)))
  (print cmd)
  (-> cmd (os.popen) (.read) (.split "\n") first))
(defn bitcoin-public-from-private [pkey] (-> "timeout 10s python3 bitcoin-in-tiny-pieces/bitcoin-public-from-private.py {}" (.format pkey) (os.popen) (.read) (.split " ") first))
(defn bitcoin-wif-private-key [seed] (-> "timeout 10s python3 bitcoin-in-tiny-pieces/bitcoin-wif-private-key.py \"{}\"" (.format (-> seed hex (.replace "x" ""))) (os.popen) (.read) (.split "\n") first (.split " ") last))
(defn get-priv-key[&optional [seed None]] 
  (setv fullkey (if (not seed) (+ "80" (-> (os.urandom 32) binascii.hexlify (.decode)))
                  (-> (urandom-seed seed 32 :mode str) (.encode "utf-8") binascii.hexlify (.decode))))
  (print fullkey)
  (setv sha256a  (-> fullkey  binascii.unhexlify hashlib.sha256 (.hexdigest)))
  (setv sha256b  (-> sha256a  binascii.unhexlify hashlib.sha256 (.hexdigest)))
  (setv WIF (-> (+ fullkey (get sha256b (slice 0 8))) binascii.unhexlify base58.b58encode))
  WIF)
(with-decorator (app.route "/") (defn index[](render-template "index.html")))

(with-decorator (app.route "/get-priv-key/") (defn gpk[] (get-priv-key)))
(defn get-wallet-btc[&optional [seed 42]]
  
  (setv priv-key (cryptos.sha256 (big-chr seed))
        addr (b.privtoaddr priv-key))
  (return {"key" priv-key "addr" addr}))
(defn get-wallet-eth[&optional [seed 42]]
  (setv acc (w3.eth.account.create (.encode (urandom-seed seed 32 :mode str) "utf-8")))
  {"key" (.hex acc.privateKey) "addr" acc.address  }
  )
(with-decorator (app.route "/get-wallet-btc/<int:seed>") 
  (defn get-wallet-btc-front[seed] (jsonify (get-wallet-btc seed))))
(with-decorator (app.route "/get-wallet-eth/<int:seed>") 
  (defn get-wallet-eth-front[seed] (jsonify (get-wallet-eth seed))))
(with-decorator (app.route "/balance/<string:addr>/")
  (defn balance[addr] (-> "timeout 10s python3 bitcoin-in-tiny-pieces/bitcoin-get-address-balance.py \"{}\" "
                          (.format addr) os.popen (.read) (.split "=") last (.split " Bitcoin") first (.replace " " "") float json.dumps)))
(defn go-to-redirect[url](render-template "redirect.html" #**{"url" url}))
(defn balance-eth[addr])
(with-decorator (app.route "/<path:path>/")
  (defn html[path]
    (render-template (if (in "." path) path (.format "{}.html" path)))))
(with-decorator (app.route "/btc/")
  (defn btc-default[] (go-to-redirect (.format "/btc/{}" (random.randint 0 BTC_LAST_PAGE)))))
(with-decorator (app.route "/eth/")
  (defn eth-default[] (go-to-redirect (.format "/eth/{}" (random.randint 0 BTC_LAST_PAGE)))))
(with-decorator (app.route "/btc/<int:page>/")
  (defn btc[&optional [page 0]]
    (render-template (if (os.path.isfile (.format "templates/btc/{}.html" page)) (.format "btc/{}.html" page) "btc.html" )
                     #**{"wallets" (list (map get-wallet-btc (range (* BTC_PER_PAGE page) (+ BTC_PER_PAGE (* BTC_PER_PAGE page)))))
                         "page" page
                         "PER_PAGE" BTC_PER_PAGE
                         "LAST_PAGE" BTC_LAST_PAGE
                         "url_monitor" "https://www.blockchain.com/btc/address"})))
(with-decorator (app.route "/eth/<int:page>/")
  (defn eth[&optional [page 0]]
    (render-template (if (os.path.isfile (.format "templates/eth/{}.html" page)) (.format "eth/{}.html" page) "eth.html" )
                     #**{"wallets" (list (map get-wallet-eth (range (* BTC_PER_PAGE page) (+ BTC_PER_PAGE (* BTC_PER_PAGE page)))))
                         "page" page
                         "PER_PAGE" BTC_PER_PAGE
                         "LAST_PAGE" BTC_LAST_PAGE
                         "url_monitor" "https://etherscan.io/address"
                         "ETHERSCAN_API" ETHERSCAN_API})))
