from hy.core.language import first, last
from flask import Flask, render_template, jsonify, redirect
import json
import random
import os
import binascii
import hashlib
import base58
import cryptos
from web3.auto import *
app = Flask('__main__', static_url_path='/static')
b = cryptos.Bitcoin()
BTC_PER_PAGE = 128
BTC_LAST_PAGE = (
    904625697166532776746648320380374280100293470930272690489102837043110636675
    )
ETHERSCAN_API = 'HUQEM3CS4AZDGBH2MKMSKYS859CPAK7NS4'


def urandom_seed(n, l=32, mode=list):
    arr = []
    for i in range(0, l):
        step = int(pow(255, l - 1 - i))
        times = int(n / step)
        if times > 0:
            arr.append(times)
            n = n - times * step
            _hy_anon_var_1 = None
        else:
            _hy_anon_var_1 = arr.append(0)
    return ''.join(list(map(chr, arr))) if mode == str else arr


def big_chr(num):
    arr = []
    while num:
        r = num % 256
        arr.append(r)
        num = num - r
        num = num / 256
    print(arr)
    return ''.join(list(map(lambda x: chr(int(x)), arr)))


def bitcoin_address_from_public_key(pkey):
    cmd = (
        'timeout 10s python3 bitcoin-in-tiny-pieces/bitcoin-address-from-public-key.py {}'
        .format(pkey))
    print(cmd)
    return first(os.popen(cmd).read().split('\n'))


def bitcoin_public_from_private(pkey):
    return first(os.popen(
        'timeout 10s python3 bitcoin-in-tiny-pieces/bitcoin-public-from-private.py {}'
        .format(pkey)).read().split(' '))


def bitcoin_wif_private_key(seed):
    return last(first(os.popen(
        'timeout 10s python3 bitcoin-in-tiny-pieces/bitcoin-wif-private-key.py "{}"'
        .format(hex(seed).replace('x', ''))).read().split('\n')).split(' '))


def get_priv_key(seed=None):
    fullkey = '80' + binascii.hexlify(os.urandom(32)).decode(
        ) if not seed else binascii.hexlify(urandom_seed(seed, 32, mode=str
        ).encode('utf-8')).decode()
    print(fullkey)
    sha256a = hashlib.sha256(binascii.unhexlify(fullkey)).hexdigest()
    sha256b = hashlib.sha256(binascii.unhexlify(sha256a)).hexdigest()
    WIF = base58.b58encode(binascii.unhexlify(fullkey + sha256b[slice(0, 8)]))
    return WIF


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/get-priv-key/')
def gpk():
    return get_priv_key()


def get_wallet_btc(seed=42):
    priv_key = cryptos.sha256(big_chr(seed))
    addr = b.privtoaddr(priv_key)
    return {'key': priv_key, 'addr': addr}


def get_wallet_eth(seed=42):
    acc = w3.eth.account.create(urandom_seed(seed, 32, mode=str).encode(
        'utf-8'))
    return {'key': acc.privateKey.hex(), 'addr': acc.address}


@app.route('/get-wallet-btc/<int:seed>')
def get_wallet_btc_front(seed):
    return jsonify(get_wallet_btc(seed))


@app.route('/get-wallet-eth/<int:seed>')
def get_wallet_eth_front(seed):
    return jsonify(get_wallet_eth(seed))


@app.route('/balance/<string:addr>/')
def balance(addr):
    return json.dumps(float(first(last(os.popen(
        'timeout 10s python3 bitcoin-in-tiny-pieces/bitcoin-get-address-balance.py "{}" '
        .format(addr)).read().split('=')).split(' Bitcoin')).replace(' ', '')))


def go_to_redirect(url):
    return render_template('redirect.html', **{'url': url})


def balance_eth(addr):
    pass


@app.route('/<path:path>/')
def html(path):
    return render_template(path if '.' in path else '{}.html'.format(path))


@app.route('/btc/')
def btc_default():
    return go_to_redirect('/btc/{}'.format(random.randint(0, BTC_LAST_PAGE)))


@app.route('/eth/')
def eth_default():
    return go_to_redirect('/eth/{}'.format(random.randint(0, BTC_LAST_PAGE)))


@app.route('/btc/<int:page>/')
def btc(page=0):
    return render_template('btc/{}.html'.format(page) if os.path.isfile(
        'templates/btc/{}.html'.format(page)) else 'btc.html', **{'wallets':
        list(map(get_wallet_btc, range(BTC_PER_PAGE * page, BTC_PER_PAGE + 
        BTC_PER_PAGE * page))), 'page': page, 'PER_PAGE': BTC_PER_PAGE,
        'LAST_PAGE': BTC_LAST_PAGE, 'url_monitor':
        'https://www.blockchain.com/btc/address'})


@app.route('/eth/<int:page>/')
def eth(page=0):
    return render_template('eth/{}.html'.format(page) if os.path.isfile(
        'templates/eth/{}.html'.format(page)) else 'eth.html', **{'wallets':
        list(map(get_wallet_eth, range(BTC_PER_PAGE * page, BTC_PER_PAGE + 
        BTC_PER_PAGE * page))), 'page': page, 'PER_PAGE': BTC_PER_PAGE,
        'LAST_PAGE': BTC_LAST_PAGE, 'url_monitor':
        'https://etherscan.io/address', 'ETHERSCAN_API': ETHERSCAN_API})

