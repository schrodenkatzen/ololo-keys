(import sys os time hy)
(while True
  (try 
    (import hy [app [app]])
    (if (= --name-- "__main__") (app.run :debug True :host "0.0.0.0" :port 80))
    (except[Exception]
      (print (sys.exc_info))
      (time.sleep 1))) 
  )